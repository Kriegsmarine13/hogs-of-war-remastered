﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour
{
    //Inputs
    public string InputForward, InputBackward, InputLeft, InputRight, InputAim, InputFire, InputInventory;
    public float speed = 6.0F;

    public int forceConst = 6;
    private bool canJump;
    private Rigidbody selfRigidBody;
    public GameObject inventoryPanel;
    public bool isInventoryOpened = false;
    public Animator animator;

    public float turnRate = 4.0F;
    public GameObject character;


    // Start is called before the first frame update
    void Start()
    {
        selfRigidBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if(canJump)
        {
            canJump = false;
            selfRigidBody.AddForce(0, forceConst, 0, ForceMode.Impulse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Inputs handling
        if(Input.GetKey(InputForward))
        {
            transform.position += transform.forward * speed * Time.deltaTime;
            Debug.Log("InputForward");
        }

        if (Input.GetKey(InputBackward))
        {
            transform.position -= transform.forward * speed * Time.deltaTime;
            Debug.Log("InputBackward");
        }

        if (Input.GetKey(InputLeft))
        {
            character.transform.Rotate(character.transform.rotation.x, character.transform.rotation.y - turnRate, character.transform.rotation.z);
            Debug.Log("InputLeft");
        }

        if (Input.GetKey(InputRight))
        {
            character.transform.Rotate(character.transform.rotation.x, character.transform.rotation.y + turnRate, character.transform.rotation.z);
            Debug.Log("InputRight");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            canJump = true;
            Debug.Log("InputJump");
        }

        if (Input.GetKey(InputAim))
        {
            Debug.Log("InputAim");
        }

        if (Input.GetKey(InputFire))
        {
            Debug.Log("InputFire");
        }

        if (Input.GetKeyDown(InputInventory))
        {
            //inventoryCanvas.getChil
            //if (!isInventoryOpened)
            //{
            if (animator.GetBool("isHidden"))
            {
                animator.SetBool("isHidden", false);
            } else
            {
                animator.SetBool("isHidden", true);
            }
            //    //Манипуляции в в закрытом инвентаре?
            //    //isInventoryOpened = true;
            //}
            //else
            //{
            //    animator.SetBool("isHidden", false);
            //    //isInventoryOpened = false;
            //}
            //Debug.Log("InputInventory");
        }
    }
}
