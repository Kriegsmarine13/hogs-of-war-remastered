﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "new Consumable", menuName = "Items/Consumable")]
public class Consumable : Item
{
    public int heal = 0;
    public int damage = 0;

    public override void Use()
    {
        GameObject player = Inventory.instance.player;

    }
}
