﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotController : MonoBehaviour
{

    public Item item;

    private void Start()
    {
        updateInfo();
    }

    public void Use()
    {
        if(item)
        {
            Debug.Log("You clicked: " + item.itemName);
        }
    }

    public void updateInfo()
    {
        Text displayText = transform.Find("Text").GetComponent<Text>();
        Image displayImage = transform.Find("Image").GetComponent<Image>();

        if (item)
        {
            displayText.text = item.itemName;
            displayImage.sprite = item.sprite;
            displayText.color = Color.clear;
        }
        else
        {
            displayText.text = "";
            displayImage.sprite = null;
            displayText.color = Color.clear;
        }
    }
}
